import matplotlib.pyplot as plt
import numpy as np
import time as t
tours = 3
precision = min(1, 0.001)
print(precision)
reduction = int (max(1, 0.01 / precision))

def verification (f, k, e) :
    k_sur = k
    bornes = megalos (f, 0.0, 2.0 * np.pi, e / 2.0)
    S = integrale (f, 0.0 ,2 * np.pi, e)
    if bornes[0] == bornes[1] :
        return(k)
    print(S / (bornes[0] - bornes[1]))
    if abs(S / (bornes[0] - bornes[1])) > 2 * np.pi / k :
        return(k)
    print (S / (bornes[0] - bornes[1]) - 2 * np.pi / k_sur)
    while (S / (bornes[0] - bornes[1]) > 2 * np.pi / k_sur) :
        k_sur = k_sur + 1
    return(k_sur)

def cart_to_pol (x, y) :
    r = (x * x + y * y) ** 0.5
    e = 1
    if x == 0 :
        if y <= 0 :
            e = -1
        return(p, e * t)
    t = np.arctan (y/x)
    if x < 0 and y < 0 :
        return(p, np. pi + t)
    if x > 0 and y < 0 :
        return(p, -t)
    if x < 0 and y > 0 :
        return( r, np.pi - t)
    return(r, t)

def fonction_angle (t) :
    return(t)
    

def fonction (t) :
    return(np.cos(t) + 1 * t)
 

def decalage_angle (t, i):
    l = len(t) - 1
    save = t[i % l] - t[(i + 1) % l]
    while l > -1:
        t[l] = t[l] + save
        l = l - 1
    return(0)

def fonction_a_liste (f,f_a, a, b, n) :
    i = 0.0
    A = float (a)
    B = float (b)
    N = float(n)
    e = (A - B) / N
    p = []
    t = []
    save = A
    
    while i < n:
        save = save + (B - A) /N
        t.append(f_a(save))
        p.append(f(f_a(save)))
        i = i + 1.0
    return(p, t)

def integrale (f, a, b, h) :
    x = 0.0
    p = [0, 0]
    i = 0.0
    
    while a + i * h < b :
        p[0] = f(a + i * h)
        p[1] = f(a + (i + 1.0) * i)
        x = x + (p[0] + p[1]) / 2.0 * (fonction_angle(a + i * h + h) - fonction_angle(a + i * h))
        i = i + 1.0
    return (x)
        


def megalos (f, a , b, e):
    t = 0
    s = abs(f(0))
    m = abs(f(0))
    while t <= 2.0 * np.pi + 0.1:
        if abs(f(t)) > s :
            s = abs(f(t))
        else :
            if abs(f(t)) < m :
                m = abs(f(t))
        t = t + e
    return([s, m])

def integrale_speciale (f, a, b, e, d):
    t = a
    s = 0
    save = 0
    while t <= b :
        save = (f(t) + f(t + e)) / 2.0 
        if d != save :
            s = s + (save / (d - save)) * (fonction_angle(t + e) - fonction_angle(t))
        t = t + e
    return(s)



def encadrer_distance_parfaite (f, k, e) :
    k_sur = verification (f, k, e)
    k_sur = k
    save = integrale_speciale(f, 0, 2 * np.pi, e, megalos(f, 0, 2 * np.pi, e)[0])
    while save < 2 * np.pi / k_sur :
        k_sur = k_sur + 1
    print(k_sur)
    bornes = [0, 0]
    M = megalos(f, 0, 2 * np.pi, e / 2) [0]
    print("le plus grand est ")
    print(M)
    S = integrale(f, 0, 2 * np.pi, e)

    bornes[1] = S * k_sur / (2 * np.pi) + M
    bornes[0] = (M + bornes[1]) / 2
    while integrale_speciale (f, 0, 2 * np.pi, e, bornes[0]) < 2 * np.pi / k_sur :
        print(bornes[0], bornes[1])
        bornes[1] = bornes[0]
        bornes[0] = (M + bornes[0]) / 2
    bornes[0] = (M + bornes[0]) / 2
    bornes[1] = bornes[1] * 2
    return([bornes, k_sur])
    

def distance_parfaite_bis (f, bornes, k, e, erreur_d) :
    print("le k definitf est")
    print(k)
    m = (bornes[1] + bornes[0]) / 2
    I = integrale_speciale(f, 0.0, 2 * np.pi, e, m)
    print(bornes, erreur_d)
    print ( 2* np.pi/k - I)
    if abs (2 * np.pi/k - I) < erreur_d :
        return([m, e, k])
    if bornes[1] - bornes[0] < 5 * e :
        e = e / 2
    if I > 2 * np.pi / k :
        return(distance_parfaite_bis(f, [m, bornes[1]], k, e,erreur_d))
    else :
        return(distance_parfaite_bis(f, [bornes[0], m], k, e, erreur_d))

    
def creer_engrenage (p, t, d, k_sur) :
    n = min(len(t) - 1, len(p) -1) 
    T = []
    P = []
    ii = 0
    save = 0
    a = 0
    maxi = 0

    while save < k_sur:
        if ii > n - 1:
            ii = 0
            save = save + 1
            print(save)
        P.append(d - p[ii])
        a = a + ((p[ii + 1] + p[ii]) / 2. /(d - ((p[ii + 1] + p[ii])/ 2)))* (t[ii + 1] - t[ii])
        T.append(-a + np.pi)
        ii = ii + 1
    return(P, T)

def polar_to_cart (u, t):
    i = 0
    l = min(len(u), len(t))
    x = []
    y = []
    while i < l:
        x.append(u[i] * np.cos(t[i]))
        y.append(u[i] * np.sin(t[i]))
        i = i + 1
    return(x, y)

def transvection (X, Y, x, y) :
    i = 0
    l = len(X)
    while i < l:
        X[i] = X[i] + x
        Y[i] = Y[i] + y
        i = i + 1
    return(0)

def reduire (x, y, fac) :
    X = []
    Y = []
    i = 0
    n = len(x)
    while i < n :
        X.append(x[i])
        Y.append(y[i])
        i = i + fac
    return(X, Y)
    
def lancer_1 (f,f_a, d, point, facteur, k_sur) :
    k = 0
    print(point)
    (p1, t1) = fonction_a_liste (f,f_a, 0, 2.0 * np.pi, point)
    (x1, y1) = polar_to_cart(p1, t1)
    plt.plot(x1, y1)
    (p2, t2) = creer_engrenage (p1, t1, d, k_sur)
    (p1, t1) = reduire(p1, t1, facteur)
    (p2, t2) = reduire (p2, t2, facteur)
    print(t2[0])
    t2.append(np.pi)
    p2.append(p2[0])
    save = len(p1) / 50.0
    m = save
    (x1, y1) = polar_to_cart(p1, t1)
    (x2, y2) = polar_to_cart(p2, t2)
    transvection(x2, y2, d, 0)
    plt.plot(x1, y1, 'r')
    plt.plot(x2, y2, 'b')
    plt.show()
    while k < 2 * len(t2):
        decalage_angle (t1, k)
        decalage_angle (t2, k)
        if m > save:
            (x1, y1) = polar_to_cart(p1, t1)
            (x2, y2) = polar_to_cart(p2, t2)
            transvection(x2, y2, d, 0)
            plt.plot(x2, y2, 'b')
            plt.plot(x1, y1, 'r')
            plt.show()
            m = 0
        k = k + 1
        m = m + 1
    return(0)

print (reduction)
help1 = encadrer_distance_parfaite(fonction, tours, precision)
dist_preci = distance_parfaite_bis (fonction, help1[0], help1[1], precision, precision * 100)
lancer_1 (fonction, fonction_angle, dist_preci[0], 2 * np.pi / dist_preci[1], reduction, dist_preci[2])   
