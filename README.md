# Presentation 

<p> Given a shape and its centre in space it is possible to recreate its "complementary shape",
 in other words another shape with a centre so that the two shapes can rotate together always having 
 a common tangent point. It is also possible in this program to decide how many turns the first shape 
 makes in relation to the second shape. <\p>

## Try it
You have to create the first shape.
The shape has to be described by two functions in polar coordinates one for the angle and one for the shape :
fonction_angle and fonction

Change the return of those functions as you like in the file tipe.py.

For example :

<pre><code>

def fonction_angle (t) :
    return(t)
    

def fonction (t) :
    return(np.cos(t) + 1 * t)

</code></pre>

Change the variable tours in the file tipe1.py.
For example 

<pre><code>
tours = 3
</code></pre>


## Run
<pre><code>
Python tipe1.py
</code></pre>
